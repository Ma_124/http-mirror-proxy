package main

import "io"

type TeeWriter struct {
	a, b io.Writer
}

func (t *TeeWriter) Write(p []byte) (n int, err error) {
	n, err = t.a.Write(p)
	if err != nil {
		return
	}

	n, err = t.b.Write(p)
	return
}
