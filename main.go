package main

import (
	"flag"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Server struct {
	cacheDir string
	client   http.Client

	upHostOverride string
	ctOverride     string

	lock sync.Mutex
}

func (s *Server) ServeHTTP(thisResp http.ResponseWriter, thisReq *http.Request) {
	defer s.lock.Unlock()
	s.lock.Lock()

	if s.upHostOverride != "" {
		thisReq.Host = s.upHostOverride
	}

	path := strings.TrimSuffix(strings.SplitN(thisReq.Host, ":", 2)[0]+"/"+strings.Trim(thisReq.URL.String(), "/"), "-")
	f, err := os.OpenFile(s.cacheDir+"/"+strings.ReplaceAll(path, "/", "-"), os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Print("error opening cache file: ", err)
		thisResp.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer func() {
		err := f.Close()
		if err != nil {
			log.Print("error closing cache file: ", err)
		}
	}()

	fi, err := f.Stat()
	if err != nil {
		log.Print("error stat-ing cache file: ", err)
		thisResp.WriteHeader(http.StatusInternalServerError)
		return
	}

	if s.ctOverride != "" {
		thisResp.Header().Set("Content-Type", s.ctOverride)
	}

	if fi.Size() == 0 {
		log.Print("proxying  ", path)

		if err := s.proxy(thisResp, &TeeWriter{thisResp, f}, thisReq, path); err != nil {
			log.Print("error proxying request: ", err)
			thisResp.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else {
		log.Print("cache hit ", path)

		thisResp.WriteHeader(http.StatusOK)
		if _, err := io.Copy(thisResp, f); err != nil {
			log.Print("error copying cached request: ", err)
			return
		}
	}
}

func (s *Server) proxy(thisResp http.ResponseWriter, body io.Writer, thisReq *http.Request, path string) error {
	upReq, err := http.NewRequest(thisReq.Method, "https://"+path, nil)
	if err != nil {
		return err
	}

	upResp, err := s.client.Do(upReq)
	if err != nil {
		return err
	}
	defer func() {
		err := upResp.Body.Close()
		if err != nil {
			log.Print(err)
		}
	}()

	thisResp.WriteHeader(upResp.StatusCode)
	if upResp.StatusCode == http.StatusOK {
		_, err = io.Copy(body, upResp.Body)
	} else {
		_, err = io.Copy(thisResp, upResp.Body)
	}

	return err
}

func main() {
	https := flag.Bool("https", false, "Enable HTTPS.")
	host := flag.String("host", "", "Host to serve on.")
	up := flag.String("up", "", "Replace Host header by this value for all requests.")
	port := flag.Int("port", 0, "Port to use. (default 8080 for HTTP, 4343 for HTTPS)")
	cert := flag.String("cert", "cert.pem", "Certificate file for HTTPS.")
	key := flag.String("key", "key.pem", "Private key file for HTTPS.")
	cache := flag.String("cache", "cache", "Dir to write files to.")
	ct := flag.String("content-type", "", "Default Content-Type to serve.")
	flag.Parse()

	if *port == 0 {
		if *https {
			*port = 4343
		} else {
			*port = 8080
		}
	}

	if err := os.Mkdir(*cache, 0777); err != nil {
		if !os.IsExist(err) {
			log.Fatal(err)
		}
	}

	client := http.Client{
		Timeout: 10 * time.Second,
	}

	s := &Server{cacheDir: *cache, client: client, upHostOverride: *up, ctOverride: *ct}

	if *https {
		log.Fatal(http.ListenAndServeTLS(*host+":"+strconv.Itoa(*port), *cert, *key, s))
	} else {
		log.Fatal(http.ListenAndServe(*host+":"+strconv.Itoa(*port), s))
	}
}
