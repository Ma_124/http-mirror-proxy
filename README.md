# http-mirror-proxy
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/Ma_124/http-mirror-proxy)](https://goreportcard.com/report/gitlab.com/Ma_124/http-mirror-proxy)

Proxy requests to an upstream server and mirror responses into a directory.

At the moment this server is quite dumb.
All methods are mapped to `GET` and all headers are removed.
Paths in the cache directory can include special characters such as `%`, `?`, `&` which are only allowed in filenames on *NIX systems.
It also cannot differentiate between `/a/b` and `/a-b`.

## Installation
```sh
go get -u gitlab.com/Ma_124/http-mirror-proxy
```

## License
Copyright &copy; 2021 Ma_124
[License](./LICENSE)

